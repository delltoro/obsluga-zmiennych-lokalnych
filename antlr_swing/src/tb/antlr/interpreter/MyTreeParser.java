package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {

	protected GlobalSymbols globalSymbols = new GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

    protected Integer declareVar(String name) {
		 globalSymbols.newSymbol(name,0);
		 return 0;
	}
    
    protected Integer declareVar(String name,Integer value) {
    	globalSymbols.newSymbol(name,value);
    	return value;
	}
    
    
	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer getValue(String i1) {
		return globalSymbols.getSymbol(i1);
	}
	
	protected Integer set(String i1, Integer e1) {
		globalSymbols.setSymbol(i1, e1);
		return e1;
	}
	
	protected Integer sum(Integer e1,Integer e2) {
		return e1+e2;
	}
	
	protected Integer sub(Integer e1,Integer e2) {
		return e1-e2;
	}
	
	protected Integer mul(Integer e1,Integer e2) {
		return e1*e2;
	}
	
	protected Integer div(Integer e1,Integer e2) {
		if(e2 == 0) {
			throw new ArithmeticException();
		}
		return e1/e2;
	}
	
}
