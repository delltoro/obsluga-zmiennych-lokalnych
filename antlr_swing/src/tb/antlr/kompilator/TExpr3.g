tree grammar TExpr3;


options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}
 
@header {
package tb.antlr.kompilator;
} 

@members {
  Integer scopeNumber = 0;
}

prog : (e += zakres | e += expr | d += decl)* -> program(name = {$e}, deklaracje = {$d});
                                  
zakres : ^(BEGIN {scopeNumber = enterScope();} (e+=zakres | e+=expr | d+=decl )*
           {scopeNumber = leaveScope();}) -> blok(wyr={$e},dekl={$d}); 
 
decl  :
         ^(VAR i1=ID)                 {addLocalVar($ID.text);} -> declareVar(i1={$ID.text+scopeNumber.toString()})
        ; 
         
expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> pomnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> podziel(p1={$e1.st},p2={$e2.st})
        | ID                       -> readVar(i1={$ID.text+scopeNumber.toString()})
        | INT      -> int(i={$INT.text},j={scopeNumber.toString()})
        |^(PODST i1=ID e2=expr)     -> setLocalVarr(s1={$ID.text+scopeNumber.toString()},p2={$e2.st})
;