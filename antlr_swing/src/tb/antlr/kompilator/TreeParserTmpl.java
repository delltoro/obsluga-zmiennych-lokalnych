/**
 * 
 */
package tb.antlr.kompilator;

import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.TreeNodeStream;
import org.antlr.runtime.tree.TreeParser;
import tb.antlr.symbolTable.LocalSymbols;
import tb.antlr.symbolTable.GlobalSymbols;

/**
 * @author tb
 *
 */
public class TreeParserTmpl extends TreeParser {

	protected GlobalSymbols globals = new GlobalSymbols();
	protected LocalSymbols local = new LocalSymbols();

	/**
	 * @param input
	 */
	public TreeParserTmpl(TreeNodeStream input) {
		super(input);
		// TODO Auto-generated constructor stub
	}
	 protected void drukuj(String text) {
	        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
	    }
	 
	protected Integer enterScope() {
		return local.enterScope();
	}
	
	protected Integer leaveScope() {
		return local.leaveScope();
	}
	
	protected String addLocalVar(String s) {
		return local.newSymbol(s);
	}
	
	
	
	protected Integer getLocalVar(String s) {
		 return local.getSymbol(s);
	}
	
	
	protected Integer getScope(String name) throws RuntimeException {
		Integer scope = local.getSymbolDepth(name);
		return scope;
	}
	
	/**
	 * @param input
	 * @param state
	 */
	public TreeParserTmpl(TreeNodeStream input, RecognizerSharedState state) {
		super(input, state);
		// TODO Auto-generated constructor stub
	}

	protected void errorID(RuntimeException ex, CommonTree id) {
		System.err.println(ex.getMessage() + " in line " + id.getLine());
	}

}
